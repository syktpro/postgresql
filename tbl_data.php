<?php
$table = $_GET['table'];
?>

<h3>
Просмотр таблицы <?=$table?>
&nbsp;&nbsp;
<a class="small" href="?page=tbl_struct&table=<?=$table?>">структура</a>
</h3>

<?php
if ($page->hasMessages()) {
	$page->printMessages();
}
?>


<?php

$limit = 10;
$start = $_GET['start'] ?: 0;
$countAll = $pdo->getData('SELECT COUNT(*) AS c FROM '.$table)[0]['c'];

$pageLinks = generatePagesLinks($limit, $start, $countAll, $floatLimit=10);

$primaryKeys = $pdo->primaryKeys($table, 1);

// Находим order
$order = $_GET['order'];
if (!$order) {
    if (count($primaryKeys)) {
    	$order = $pks[0];
    }
}

// Составляем запрос
$sql = 'SELECT * FROM '.$table.'';
if ($order) {
	$sql .= ' ORDER BY "'.$order.'"';
}
if ($_GET['order-desc']) {
	$sql .= ' DESC';
}
$sql .= ' LIMIT '.$limit.' OFFSET '.$start;

// Извлекаем данные
$data = $pdo->getData($sql);


if (!$data) {
    echo 'Нет данных';
    return ;
}

$fields = array_keys($data[0]);


?>


<table class="table table-pg">
<tr>
    <th>&nbsp;</th>
<?php
foreach ($fields as $field) {
    $add = '';
    if ($field == $_GET['order']) {
        if ($_GET['order-desc']) {
        	$add = '&order-desc=';
        } else {
        	$add = '&order-desc=1';
        }
    } else {
        if ($_GET['order-desc']) {
        	$add = '&order-desc=';
        }
    }
    echo '<th><a href="'.url('order='.$field.$add).'">'.$field.'</a></th>';
}
?>
</tr>
<?php
foreach ($data as $row) {
    $where = [];
    foreach ($primaryKeys as $pk) {
    	$where []= '"'.$pk.'"=\''.$row[$pk].'\'';
    }
    $where = implode(' AND ', $where);
    ?>
    <tr>
        <td>
            <a title="Удаление строки" href="?page=tbl_data&action=deleteRow&table=<?=$table?>&where=<?=urlencode($where)?>"><i class="glyphicon glyphicon-remove"></i></a>
            &nbsp;
            <a title="Редактирование строки" href="?page=tbl_row&table=<?=$table?>&where=<?=urlencode($where)?>"><i class="glyphicon glyphicon-edit"></i></a>
            &nbsp;
        </td>
    <?php
    foreach ($fields as $field) {
        echo '<td>'.$row[$field].'</td>';
    }
    ?>
    </tr>
    <?php
}
?>
</table>

<?php
echo $pageLinks;
?>