<?php

$table = $_GET['table'];

?>

<h3>Структура таблицы <?=$table?>
&nbsp;&nbsp;
<a class="small" href="?page=tbl_data&table=<?=$table?>">данные</a>
</h3>


<?php
if ($page->hasMessages()) {
	$page->printMessages();
    $page->redirect(BASE_URL.'?page=tbl_struct&table='.$table, 1);
    return ;
}
?>

<?php
$fields = $pdo->listFields($table);
?>

<a href="?page=tbl_edit&table=<?=$table?>&mode=addField" class="btn btn-info">Добавить поле</a>

<hr />

<table class="table table-pg">
<tr>
    <th>Поле</th>
    <th>Тип поля</th>
    <th></th>
</tr>
<?php
foreach ($fields as $field) {
    ?>
    <tr>
        <td>
            <a title="Удаление поля" onclick="if (!confirm('Вы уверены, что хотите удалить поле <?=$field['column_name']?>?')) return false;" href="?page=tbl_struct&action=dropColumn&table=<?=$table?>&column=<?=$field['column_name']?>"><i class="glyphicon glyphicon-remove"></i></a>
            &nbsp;
            <a title="Редактирования поля" href="?page=tbl_edit&table=<?=$table?>&field=<?=$field['column_name']?>"><i class="glyphicon glyphicon-edit"></i></a>
            &nbsp;
        </td>
        <td><?=$field['column_name']?></td>
        <td><?=$field['data_type']?></td>
    </tr>
    <?php
}
?>

</table>