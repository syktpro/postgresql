<?php
$data = $pdo->listTablesFull();
?>

<h3>Список таблиц базы данных <?=DB_NAME?></h3>

<?php
if ($page->hasMessages()) {
	$page->printMessages();
    $page->redirect(BASE_URL, 1);
    return ;
}
?>

<table class="table table-pg">
<?php
foreach ($data as $v) {
    $table_name = $v['relname'];
    $rows = $v['reltuples'];
?>
<tr>
    <td><a href="?page=tbl_data&table=<?=$table_name?>"><?=$table_name?></a></td>
    <td class="small text-right"><?=$rows?></td>
    <td>
    <a title="Структура таблицы" href="?page=tbl_struct&table=<?=$table_name?>"><i class="glyphicon glyphicon-list-alt"></i></a>
    &nbsp;
    <a title="Удаление таблицы" onclick="if (!confirm('Вы уверены, что хотите удалить таблицу <?=$table_name?>?')) return false;" href="?action=deleteTable&table=<?=$table_name?>"><i class="glyphicon glyphicon-remove"></i></a>
    &nbsp;
    <a title="Очистка таблицы" onclick="if (!confirm('Вы уверены, что хотите очистить таблицу <?=$table_name?>?')) return false;" href="?action=truncateTable&table=<?=$table_name?>"><i class="glyphicon glyphicon-trash"></i></a>
    &nbsp;
    <a title="Копирование структуры таблицы" onclick="if (name = prompt('Введите названия таблицы', '<?=$table_name?>')) location = '?action=copyTable&table=<?=$table_name?>&name='+name; return false;" href="#"><i class="glyphicon glyphicon-copy"></i></a>
    </td>
</tr
<?php
}
?>
</table>